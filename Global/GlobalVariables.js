"use strict";
exports.__esModule = true;
exports.globalVariables = exports.loginCreds = void 0;
exports.loginCreds = {
    url: 'https://release2022120.a.karve.dev/auth/login',
    account: 'support@karveit.ca',
    password: '{00P}model'
};
exports.globalVariables = {
    browser: 'chrome',
    passwordID: 'password',
    loginID: 'login-button',
    accountXPath: '/html/body/app-root/app-login/div/div/div[1]/div[1]/input',
    checkDashboardXPath: '/html/body/app-root/app-main/div/div[1]/div/app-dashboard/div/div/div[1]/h2'
};
