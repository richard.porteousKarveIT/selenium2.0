export interface LoginCreds {
    url: string;
    account: string;
    password: string;
}

export interface GlobalVariables {
    browser: string;
    passwordID: string;
    loginID: string;
    accountXpath: string;
    checkDashboardXpath: string;
    root: string,
    htmlBodyXPath: string,
    mutateXPath: string,
    mutateContainterXPath: string,
    rightPanelXPath: string,
    topBarXPath: string,
    loginXPath: string

}

export const loginCreds = {
    url: 'https://release2022120.a.karve.dev/auth/login',
    account: 'support@karveit.ca',
    password: '{00P}model'
}

export const globalVariables = {
    browser: 'chrome',
    passwordID: 'password',
    loginID: 'login-button',
    accountXPath: '/html/body/app-root/app-login/div/div/div[1]/div[1]/input',
    checkDashboardXPath: '/html/body/app-root/app-main/div/div[1]/div/app-dashboard/div/div/div[1]/h2',
    root: 'html/body/app-root/app-',
    htmlBodyXPath: '/html/body/',
    mutateXPath: '/app-mutate-object/p-dialog/div/div',
    mutateContainterXPath: 'mutate-container/p-dialog/div/div/',
    rightPanelXPath: 'main/div/app-rightpanel/div/div/app-',
    topBarXPath: 'main/top_bar/div/',
    loginXPath: 'login/div/div/div[1]/div[1]/input',
}
