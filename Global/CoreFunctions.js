"use strict";
exports.__esModule = true;
exports.CoreFunctions = void 0;
var selenium_webdriver_1 = require("selenium-webdriver");
var GlobalVariables_1 = require("./GlobalVariables");
var driver = new selenium_webdriver_1.Builder().forBrowser(GlobalVariables_1.globalVariables.browser).build();
var CoreFunctions = /** @class */ (function () {
    function CoreFunctions() {
        globalThis.driver = driver;
    }
    CoreFunctions.prototype.go_to_url = function (theUrl) {
        driver.get(theUrl);
    };
    return CoreFunctions;
}());
exports.CoreFunctions = CoreFunctions;
