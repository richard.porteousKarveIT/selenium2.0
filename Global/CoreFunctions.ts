import { Builder, By, Key } from 'selenium-webdriver';
import { globalVariables } from './GlobalVariables';

const driver = new Builder().forBrowser(globalVariables.browser).build();

export class CoreFunctions {
    constructor() {
        globalThis.driver = driver;
    }
    go_to_url(theUrl: string) {
        driver.get(theUrl);
    }
    check_text(targetXPath) {
        driver
            .findElement(By.xpath(targetXPath))
            .getText()
            .then(function (value) {
                return value;
            });
    }
    send_text_by_xpath(targetXPath, inputText) {
        driver.findElement(By.xpath(targetXPath)).sendKeys(inputText);
    }

    send_text_and_enter_by_id(targetID, inputText) {
        driver
            .findElement(By.id(targetID))
            .sendKeys(inputText, Key.ENTER);
    }

    click_by_xpath(targetXPath) {
        driver.findElement(By.xpath(targetXPath)).click();
    }

    selenium_close() {
        driver.close();
    }
}