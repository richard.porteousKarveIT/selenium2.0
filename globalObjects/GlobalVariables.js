//declarre global variables

class GlobalVariables {
  inputLogin = "admin@youmoveme.com";
  inputPassword = "Aw9PEpBgh0Tg";
  devRoot = "https://release2022110.a.karve.dev/";

  chromeBrowser = "chrome";

  root = "html/body/app-root/app-";
  htmlBodyXPath = "/html/body/";
  mutateXPath = "/app-mutate-object/p-dialog/div/div";
  mutateContainterXPath = "mutate-container/p-dialog/div/div/";
  rightPanelXPath = "main/div/app-rightpanel/div/div/app-";
  topBarXPath = `${this.root}main/top_bar/div/`;

  loginXPath = `${this.root}login/div/div/div[1]/div[1]/input`;
  passwordID = "password";
  loginButton = "login-button";
}

//export global variables.
module.exports = new GlobalVariables();
