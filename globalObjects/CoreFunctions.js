//import modules
const webdriver = require("selenium-webdriver");
const { By } = require("selenium-webdriver");
const driver = new webdriver.Builder().forBrowser("chrome").build();

//class to export the functions
class CoreFunctions {
  constructor() {
    global.driver = driver;
  }

  //all the individual functions responsible for actions are labeled using_snake_case for ease of viewing
  go_to_url(theURL) {
    driver.get(theURL);
  }

  check_text(targetXPath) {
    driver
      .findElement(By.xpath(targetXPath))
      .getText()
      .then(function (value) {
        return value;
      });
  }

  send_text_by_xpath(targetXPath, inputText) {
    driver.findElement(By.xpath(targetXPath)).sendKeys(inputText);
  }

  send_text_and_enter_by_id(targetID, inputText) {
    driver
      .findElement(By.id(targetID))
      .sendKeys(inputText, webdriver.Key.ENTER);
  }

  click_by_xpath(targetXPath) {
    driver.findElement(By.xpath(targetXPath)).click();
  }

  selenium_close() {
    driver.close();
  }
}

//export the modules
module.exports = new CoreFunctions();
