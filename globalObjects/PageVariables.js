//import modules
const GlobalVariables = require("./GlobalVariables");

//declare pages
const Pages = {
  mainPage: `${GlobalVariables.devRoot}`,
  jobsPage: `${GlobalVariables.devRoot}/jobs`,
  availabilityPage: `${GlobalVariables.devRoot}/availability`,
  schedulePage: `${GlobalVariables.devRoot}/schedule`,
  userPage: `${GlobalVariables.devRoot}/users`,
  productsPage: `${GlobalVariables.devRoot}/products`,
  assetPage: `${GlobalVariables.devRoot}/assets`,
  estimatingPage: `${GlobalVariables.devRoot}/estimating?step=customer`,
  discountsPage: `${GlobalVariables.devRoot}/marketing/discounts`,
  franchiseManagementPage: `${GlobalVariables.devRoot}/franchise-management`,
};

//class variable to export the page variables
class PageVariables {
  constructor(name, root, url, check, checkValue, checkXPath) {
    name = this.name;
    root = this.root;
    url = this.url;
    check = this.check;
    checkValue = this.checkValue;
    checkXPath = this.checkXPath;
  }
}
// these initiate a new itteration of the PageVariables class for export
const AvailabilityVariables = new PageVariables(
  "availability",
  GlobalVariables.root,
  Pages.availabilityPage,
  "main/div/div[1]/div/app-availability/div[1]/div/div[1]/h4",
  "Availability",
  `${this.root}${this.check}`
);

const DashboardVariables = new PageVariables(
  "main",
  GlobalVariables.root,
  Pages.mainPage,
  "main/div/div[1]/div/app-dashboard/div/div/div[1]/h2",
  "Dashboard",
  `${this.root}${this.check}`
);

//export the modules
module.exports = { DashboardVariables, AvailabilityVariables, Pages };
