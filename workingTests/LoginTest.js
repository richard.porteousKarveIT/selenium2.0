//variables and function modules
const {
  go_to_url,
  send_text_and_enter_by_id,
  send_text_by_xpath,
} = require("../globalObjects/CoreFunctions");

const {
  devRoot,
  loginXPath,
  inputLogin,
  passwordID,
  inputPassword,
} = require("../globalObjects/GlobalVariables");

//import the test into a class to use globaly;
class Tests {
  constructor() {
    global.loginTest = LoginTest();
  }
}

//go the the login page, locate the login input and send the login account, locate the password input,
//send the password, and send enter to login.
function LoginTest() {
  go_to_url(devRoot);
  send_text_by_xpath(loginXPath, inputLogin);
  send_text_and_enter_by_id(passwordID, inputPassword);
}

//export the test
module.exports = new Tests();
