'use strict';
var __makeTemplateObject =
  (this && this.__makeTemplateObject) ||
  function (cooked, raw) {
    if (Object.defineProperty) {
      Object.defineProperty(cooked, 'raw', { value: raw });
    } else {
      cooked.raw = raw;
    }
    return cooked;
  };
exports.__esModule = true;
var client_1 = require('@apollo/client');
var Query_1 = require('./src/Query');
var Variables_1 = require('./src/Variables');
var client = new client_1.ApolloClient({
  uri: Variables_1.variables.environment.production.url,
  cache: new client_1.InMemoryCache(),
});
client
  .query({
    query: (0, client_1.gql)(
      templateObject_1 ||
        (templateObject_1 = __makeTemplateObject(['', ''], ['', ''])),
      Query_1.query
    ),
    variables: Variables_1.variables.selectedVariables,
  })
  .then(function (result) {
    return console.log(result);
  });
var templateObject_1;
