
import { ApolloClient, InMemoryCache, ApolloProvider, gql, HttpLink } from "@apollo/client";
import fetch from "cross-fetch";
import { input } from "./src/Input";
import { variables } from "./src/Variables";

const client = new ApolloClient({
    link: new HttpLink({
        uri: variables.environment.production.url,
        fetch,
    }),
    cache: new InMemoryCache()
});
client.query({
    query: gql`${input.query}`,
    variables: input.variables,

})
    .then(result => console.log(result));