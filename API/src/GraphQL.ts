import { variables } from './Variables';
import { input, Input } from './Input';
import { gql, useQuery } from '@apollo/client';


export function runAPI() {

    if (variables.environment.selected == 'production') {
        let url = variables.environment.production.url;
        let pass = variables.environment.production.pass;
        let login = variables.environment.production.login;
        callAPI(url, login, pass, input);
    }

    if (variables.environment.selected == 'staging') {
        let url = variables.environment.staging.url;
        let pass = variables.environment.staging.pass;
        let login = variables.environment.staging.login;
        callAPI(url, login, pass, input);
    }


    if (variables.environment.selected == 'devops') {
        let url = variables.environment.devops.url;
        let pass = variables.environment.devops.pass;
        let login = variables.environment.devops.login;
        callAPI(url, login, pass, input);
    }
}

export function callAPI(url: string, pass: string, login: string, input: Input) {
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${login}:${pass}`
    };
    if (input.zone) {
        headers['x-zone'] = input.zone;
    } else if (input.zoneId) {
        //call avaialble zones
        //set x-zone to zone with the name, if that zone does not exist throw an error
        //headers['x-zone'] = zoneName;
    }
    const body = {
        query: input.query,
        variables: input.variables
    };
    const options = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body)
    };
    fetch(url, options)
        .then(response => response.json())
        .then(data => console.log(data));
}
