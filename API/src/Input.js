"use strict";
exports.__esModule = true;
exports.input = void 0;
var Query_1 = require("./Query");
var Variables_1 = require("./Variables");
exports.input = {
    query: Query_1.query.selected,
    variables: Variables_1.variables.selectedVariables,
    zone: Variables_1.variables.selectedZone,
    zoneId: Variables_1.variables.selectedZoneId
};
