"use strict";
exports.__esModule = true;
exports.callAPI = exports.runAPI = void 0;
var Variables_1 = require("./Variables");
var Input_1 = require("./Input");
function runAPI() {
    if (Variables_1.variables.environment.selected == 'production') {
        var url = Variables_1.variables.environment.production.url;
        var pass = Variables_1.variables.environment.production.pass;
        var login = Variables_1.variables.environment.production.login;
        callAPI(url, login, pass, Input_1.input);
    }
    if (Variables_1.variables.environment.selected == 'staging') {
        var url = Variables_1.variables.environment.staging.url;
        var pass = Variables_1.variables.environment.staging.pass;
        var login = Variables_1.variables.environment.staging.login;
        callAPI(url, login, pass, Input_1.input);
    }
    if (Variables_1.variables.environment.selected == 'devops') {
        var url = Variables_1.variables.environment.devops.url;
        var pass = Variables_1.variables.environment.devops.pass;
        var login = Variables_1.variables.environment.devops.login;
        callAPI(url, login, pass, Input_1.input);
    }
}
exports.runAPI = runAPI;
function callAPI(url, pass, login, input) {
    var headers = {
        'Content-Type': 'application/json',
        'Authorization': "Basic ".concat(login, ":").concat(pass)
    };
    if (input.zone) {
        headers['x-zone'] = input.zone;
    }
    else if (input.zoneId) {
        //call avaialble zones
        //set x-zone to zone with the name, if that zone does not exist throw an error
        //headers['x-zone'] = zoneName;
    }
    var body = {
        query: input.query,
        variables: input.variables
    };
    var options = {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body)
    };
    fetch(url, options)
        .then(function (response) { return response.json(); })
        .then(function (data) { return console.log(data); });
}
exports.callAPI = callAPI;
