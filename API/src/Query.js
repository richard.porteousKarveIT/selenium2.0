"use strict";
exports.__esModule = true;
exports.query = void 0;
var queries = {
    getProducts: "query\n    query products {\n      products {\n        products{\n          id,\n          name\n        }\n      }\n    }\n"
};
exports.query = {
    selected: queries.getProducts
};
