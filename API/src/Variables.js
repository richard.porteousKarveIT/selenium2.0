"use strict";
exports.__esModule = true;
exports.variables = void 0;
exports.variables = {
    selectedVariables: [''],
    selectedZone: '',
    selectedZoneId: '',
    environment: {
        selected: 'production',
        staging: {
            url: 'https://release202213.a.karve.dev/users',
            login: 'admin@youmoveme.com',
            pass: 'UEeowJRwV4pO'
        },
        production: {
            url: 'https://app.youmoveme.com/api/graphql',
            login: 'support@karveit.ca',
            pass: 'HN#2nsGwc[7ehx=l,$iv'
        },
        devops: {
            url: 'https://dbtest4.a.karve.dev/products',
            login: 'admin@youmoveme.com',
            pass: 'kskHDxK489cn'
        }
    },
    keys: ['on', 'seconds', 'factor', 'products'],
    eventType: ['moving', 'packing', 'unpacking', 'delivery', 'estimating'],
    locationType: ['basestart', 'start', 'end'],
    quantityVariable: ['event-total-time', 'event-total-locationtime', 'event-total-traveltime', 'event-total-distance-mile', 'event-total-distance-km'],
    franchiseLocation: ['Toledo', 'Phoenix', 'Salt Lake City', 'St Louis'],
    roundingFactor: ['round', 'ceil', 'floor'],
    dwellingType: ['Apartment', 'Condo', 'House', 'Commercial', 'Other'],
    elevators: ['None', 'Passenger', 'Freight'],
    stairs: ['None', '1 Flight', '2 Flight', '3 Flight', '4 Flight', '5 Flight'],
    numberOfBedrooms: ['Bachelor/Studio', '1', '2', '3', '4', '5+'],
    categories: ['General', 'Supplies', 'Labor', 'Protection Plans'],
    colors: ['Mint', 'Red', 'Lavender', 'Green', 'Pink', 'Beige', 'Teal', 'Maroon']
};
