import { query } from "./Query";
import { variables } from "./Variables";


export interface Input {
    query: string;
    variables: [string];
    zone: string;
    zoneId: string;
}

export const input: Input = {
    query: query.selected,
    variables: variables.selectedVariables,
    zone: variables.selectedZone,
    zoneId: variables.selectedZoneId,
}