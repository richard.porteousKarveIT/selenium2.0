import { CoreFunctions } from "../Global/CoreFunctions";
import { globalVariables, loginCreds } from "../Global/GlobalVariables";

let coreFunctions = new CoreFunctions

export class LoginTest {
    constructor() {
        async function loginTest() {
            coreFunctions.go_to_url(loginCreds.url);
            await coreFunctions.send_text_by_xpath(globalVariables.accountXPath, loginCreds.account);
            await coreFunctions.send_text_and_enter_by_id(globalVariables.loginID, loginCreds.password);
        }
    }
}