"use strict";
exports.__esModule = true;
function configureLog4js(obj) {
    return 'type' in obj && obj.type === 'Configure';
}
var log4js = {
    appenders: {
        selenium: {
            type: "file",
            filename: "selenium.log"
        }
    },
    categories: {
        "default": {
            appenders: [
                "selenium"
            ],
            level: "debug"
        }
    },
    type: 'Configure'
};
